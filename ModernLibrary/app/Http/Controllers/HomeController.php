<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Loan;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $book = Book::where('tampil', 1)->get();
        $jmlbook = Book::count();
        $jmlcat = Category::count();
        $loan = Loan::all();
        if (Auth::user()->role == "admin") {
            Alert::success('Selamat Datang', 'Admin');
            return view('layouts/beranda', compact('book', 'jmlbook', 'jmlcat','loan'));
        }elseif (Auth::user()->role == "customer") {
            Alert::success('Selamat Datang', 'Customer');
            return view('layouts/beranda', compact('book', 'jmlbook', 'jmlcat','loan'));
        }
    }
    
}
