@extends('layouts.template')

@section('title', 'Data Kategori')
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Data /</span> Kelola Kategori</h4>

    <!-- Basic Bootstrap Table -->
    <div class="card">
        <h5 class="card-header">Kelola Kategori Buku</h5>
        <div class="table-responsive">
            <a href="{{ route('category.create') }}" class="btn btn-primary ms-3">Tambah Data</a>
            <table class="table" id="myTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kategori</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    @foreach ($category as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->nama_kategori }}</td>
                        <td>
                            <div class="dropdown">
                                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="bx bx-dots-vertical-rounded"></i></button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item btn btn-primary" href="{{ route('category.edit', $item->id) }}"><i class="bx bx-edit-alt me-1"></i>Edit</a>
                                    <form action="{{ route('category.destroy', $item -> id) }}" method="POST" id="myForm">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="dropdown-item btn btn-danger" onclick="return confirm('Yakin hapus Data ?')"><i class="bx bx-trash me-1"></i>Delete</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

@endsection
