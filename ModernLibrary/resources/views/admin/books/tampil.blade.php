@extends('layouts.template')

@section('title', 'Data Kategori')
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Data /</span> Kelola Kategori</h4>

    <!-- Basic Bootstrap Table -->
    <div class="card">
        <h5 class="card-header">Kelola Kategori Buku</h5>
        <div class="table-responsive">
            <a href="{{ route('books.create') }}" class="btn btn-primary ms-3">Tambah Data</a>
            <table class="table" id="myTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ISBN</th>
                        <th>Judul Buku</th>
                        <th>Penerbit</th>
                        <th>Sinopsis</th>
                        <th>Keterangan</th>
                        <th>Kategori</th>
                        <th>Stok</th>
                        <th>Cover</th>
                        <th>Aksi</th>

                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    @foreach ($book as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->isbn }}</td>
                        <td>{{ $item->judul_buku }}</td>
                        <td>{{ $item->penerbit }}</td>
                        <td>{{ $item->sinopsis }}</td>
                        <td>{{ $item->keterangan }}</td>
                        <td>{{ $item->category->nama_kategori }}</td>
                        <td>{{ $item->stock }}</td>
                        <td><img src="{{ asset('storage/' . $item->cover) }}" alt="" width="100px"></td>
                        <td class="d-flex">
                                    <a class="btn btn-warning" href="{{ route('books.edit', $item->id) }}"><i class="bx bx-edit-alt me-1"></i></a>
                                    <form action="{{ route('books.destroy', $item -> id) }}" method="POST" id="myForm">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger ms-3" onclick="return confirm('Yakin hapus Data ?')"><i class="bx bx-trash me-1"></i></button>
                                    </form>
                                    @if (Auth::user()->role == "admin")
                                @if ($item->tampil == 1)
                                    <a href="/tampil/{{ $item->id }}" class="btn btn-primary ms-3">Sembunyi</a>
                                @else
                                    <a href="/tampil/{{ $item->id }}" class="btn btn-warning ms-3">Tampil</a>
                                @endif
                            @endif
                                
                        </td>
                       
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

@endsection
