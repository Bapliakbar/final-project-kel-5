@extends('layouts.template')

@section('title', 'Edit buku')

@section('content')
    <form action="/books/{{ $book->id }} " method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Data /</span> <span
                    class="text-muted fw-light">Kelola Buku /</span> Edit Jenis Buku</h4>

            <div class="col-xxl">
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="{{ route('books.update', $book->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group mb-3">
                                <label>ISBN</label>
                                <input type="text" class="form-control" value="{{ $book->isbn }} " name="isbn"
                                    placeholder="Masukan isbn">
                                @error('isbn')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label>Judul Buku</label>
                                <input type="text" class="form-control" value="{{ $book->judul_buku }}" name="judul_buku"
                                    placeholder="Masukan Title">
                                @error('judul_buku')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label>Penerbit</label>
                                <input type="text" class="form-control" value="{{ $book->penerbit }}" name="penerbit"
                                    placeholder="Masukan Penerbit">
                                @error('penerbit')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label>Sinopsis</label>
                                <input type="text" class="form-control"value="{{ $book->sinopsis }}" name="sinopsis"
                                    placeholder="Masukan Sinopsis">
                                @error('sinopsis')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label>Keterangan</label>
                                <input type="text" class="form-control" value="{{ $book->keterangan }}"name="keterangan"
                                    placeholder="Masukan Keterangan">
                                @error('keterangan')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group mb-3">
                                <label>Stock</label>
                                <input type="text" class="form-control" value="{{ $book->stock }}" name="stock"
                                    placeholder="Masukan Stock">
                                @error('stock')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label>Category</label>
                                <select name="category_id" class="form-select" id="exampleFormControlSelect1">
                                    @foreach ($category as $data)
                                        <option value="{{ $data->id }}" @selected($book->category_id == $data->id)>
                                            {{ $data->nama_kategori }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group mb-3">
                                <label>Cover</label>
                                <input type="file" class="form-control" name="cover">
                                @error('cover')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Cover Sebelumnya : </label>
                            </div>
                            <div class=" form-group mb-3">
                                <img src="{{ asset('storage/' . $book->cover) }}" width="200px" alt="">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <div class="row justify-content-end">
                                {{-- <div class="col-sm-15"> --}}
                                {{-- </div> --}}
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
