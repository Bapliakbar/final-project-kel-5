@extends('layouts.template')

@section('title', 'Tambah buku')

@section('content')
<form action="/books" method="POST" enctype="multipart/form-data">
@csrf
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Data /</span> <span class="text-muted fw-light">Kelola Buku /</span> Tambah Jenis Buku</h4>

    <div class="col-xxl">
        <div class="card mb-4">
            <div class="card-body">
            <div class="form-group mb-3">
                <label>ISBN</label>
                <input type="text" class="form-control" name="isbn" placeholder="Masukan isbn">
                @error('isbn')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group mb-3">
                <label>Judul Buku</label>
                <input type="text" class="form-control" name="judul_buku" placeholder="Masukan Title">
                @error('judul_buku')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group mb-3">
                <label>Penerbit</label>
                <input type="text" class="form-control" name="penerbit" placeholder="Masukan Penerbit">
                @error('penerbit')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group mb-3">
                <label>Sinopsis</label>
                <input type="text" class="form-control" name="sinopsis" placeholder="Masukan Sinopsis">
                @error('sinopsis')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group mb-3">
                <label>Keterangan</label>
                <input type="text" class="form-control" name="keterangan" placeholder="Masukan Keterangan">
                @error('keterangan')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group mb-3">
                <label>Cover</label>
                <input type="file" class="form-control" name="cover">
                @error('cover')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group mb-3">
                <label>Stock</label>
                <input type="text" class="form-control" name="stock" placeholder="Masukan Stock">
                @error('stock')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group mb-3">
                <label>Kategori</label>
                <select name="category_id" class="form-control" id="">
                    <option value="">--Pilih Category--</option>
                    @forelse ($category as $item)
                    <option value="{{$item->id}}">{{$item->nama_kategori}} </option>
                    @empty
                    <option value="">Tidak ada Data di table Category</option>
                    @endforelse
                </select>
                {{-- @error('category_id')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror --}}
            </div>
            
            <button type="submit" class="btn btn-primary">Submit</button>
            <div class="row justify-content-end">
               {{-- <div class="col-sm-15"> --}}
                {{-- </div> --}}
            </div>
        </div>
        </div>
    </div>
</div>
</form>
@endsection